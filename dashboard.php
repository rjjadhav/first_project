<?php include('connection.php'); 
session_start();?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/index1.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
 	<link rel="stylesheet" href="css/bootstrap.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid"> 
    <ul class="nav navbar-nav">
      <li><a href="#"><div class="glyphicon glyphicon-th"></div></a></li>
      <li class="active"><a href="dashboard.php" target="_self">Dashboard</a></li>
      <li><a href="dash.php" target="_self">Valve</a></li>    
    </ul>
  </div>
</nav>

<header id="heading">
	<h1><strong>VALVE MANAGEMENT SYSTEM DASHBOARD</strong></h1>
</header>

<div class="row">

<div class="lcnright">
<strong style="float:right; margin-right:220px; margin-top:20px; font-size:18px;">LOCATION:</strong>
<select class="form-control optn" name='loc' id='loc' onchange="dynamic_location();">
        <option>--Select Location--</option>

        <?php 
                        $dg_query = "select * from tbl_location";
                        $query_result=mysql_query($dg_query); 
                          $num_rows = mysql_num_rows($query_result);
                
                       if($num_rows > 0) {
                          while($result=mysql_fetch_array($query_result)) { ?>
                        <option value='<?php echo $result['location_id']; ?>'><?php echo $result['location_name']; ?> </option>
                      <?php } 
                       } ?>
        
      </select>
      <br>
      <br>
      <strong style="float:right; margin-right:-65px; margin-top:20px; font-size:18px;">VALVE:</strong>
      <select class="form-control optn" name='subloc' id='subloc' onchange="locationpara_location()" >
        <option>--Select SubLocation--</option>
      </select>
       <br>
</div>

<h4 class="location"><strong>LOCATION PARAMETERS</strong></h4>
<h4 class="operation"><strong>VALVE OPERATION</strong></h4>
	<div class="col-sm-4 leftside">

	<div class="row">
		<div class="col-sm-5" id="lsl">
		<strong>TANK LEVEL<br><br>
			FLOW RATE 1<br><br>
			FLOW RATE 2<br><br>
			LINE PRESSURE 1<br><br>
			LINE PRESSURE 2<br><br>
			RECORD TIME</strong>
		</div>

		<div class="col-sm-5 lsr">
		<strong>
		<input type="text" id="tank_level" name="tanklevel" style="width:60px; border:none; border-bottom:2px solid black; background-color:#C9F36C;">Mtr.</input><br><br>

		<input type="text" id="flow_rate1" name="tanklevel" style="width:50px; border:none; border-bottom:2px solid black; background-color:#C9F36C;">M3/Hr.</input><br><br>

		<input type="text" id="flow_rate2" name="tanklevel" style="width:50px; border:none; border-bottom:2px solid black; background-color:#C9F36C;">M3/Hr.</input><br><br>

		<input type="text" id="line_pressure1" name="tanklevel" style="width:45px; border:none; border-bottom:2px solid black; background-color:#C9F36C;">Kg/Cm2</input><br><br>

		<input type="text" id="line_pressure2" name="tanklevel" style="width:45px; border:none; border-bottom:2px solid black; background-color:#C9F36C;">Kg/CM2</input><br><br>

		<input type="text" id="record_time" name="tanklevel" style="width:45px; border:none; border-bottom:2px solid black; background-color:#C9F36C;">/Hr</input><br><br>

		</strong>
		</div>
	</div>	
	</div>

	<div class="col-sm-3 middle">
	<h4 id="level1"><strong>VALVE1</strong></h4>
	   <div class="level1">
		  PERCENTAGE OF VALVE OPENING<br><br>

		  <input id="XX" type="text" id="valve_per" name="XX%" value="XX%"></input><br><br>
		  <button  type="button" id="valve_open1" class=" op btn btn-success " >OPEN</button>
		  <button type="button" id="valve_close1" class="btn btn-danger cp" >CLOSE</button>
 
	   </div>
		 <div class="vop">
				   VOLVE OPEN PERCENTAGE
		 </div>
		      <div class="vcp">
		       	VOLVE CLOSE PERCENTAGE
	      	</div>

             <strong><input type="text" class="pervop" id="valveopr_open1" value="05"></input></strong>
   
             <strong><input type="text" class="pervcp" id="valveopr_close1" value="10"></input></strong>

          </div>


	<div class="col-sm-3 rightside">
	<h4 id="level2" ><strong>VALVE2</strong></h4>	

	<div class="level2">

		PERCENTAGE OF VALVE OPENING<br><br>
		<input id="XX1" type="text" name="XX%" value="XX%"></input><br><br>
		<button type="button" id="valve_open2" class="btn btn-success op" >OPEN</button>
		<button type="button" id="valve_close2" class="btn btn-danger cp" >CLOSE</button>
	</div>

	<div class="vop">
			
				VOLVE OPEN PERCENTAGE
	</div>

		<div class="vcp">
			VOLVE CLOSE PERCENTAGE
		</div>

		<strong><input type="text" class="pervop" id="valveopr_open2" value="05"></input></strong>
		
	  <strong><input type="text" class="pervcp" id="valveopr_close2" value="10"></input></strong>
		
	</div>
</div>

<script>
      function dynamic_location(){
          var location_id = $('#loc').val();

          var get_location_name = $.ajax({
            type:"GET",
            url: "database.php?location_id="+location_id,
            success: function(result) {
              $("#subloc").html(result);
             
              $('#subloc').val($('#hdn_sublocation_name').val());
            }
          });
      }


$("button").click(function() {
    var id =this.id;
    var valveopr_id =id.charAt(id.length-1);

         if (id=="valve_open1") {
         
            var get_location_name = $.ajax({
            type:"GET",
            url: "valveop.php?valveopr_id="+valveopr_id,
            success: function(result) {
              
            	var res=JSON.parse(result).info;
              
              $("#valveopr_open1").val(res[0].valveopr_open1);
             
             

              var op1=res[0].valveopr_open1;
              var cl1=res[0].valveopr_close1;
               
              var per=((parseInt(op1) + parseInt(cl1))/2);
              document.getElementById("XX").value=per;

              
                $("#XX").val(per);
              localStorage.setItem("XX",per);
              
            }
          });

          }

          else if(id=="valve_open2")
          {

            var get_location_name = $.ajax({
            type:"GET",
            url: "valveop.php?valveopr_id="+valveopr_id,
            success: function(result) {
              
              var res=JSON.parse(result).info;
             
             $("#valveopr_open2").val(res[0].valveopr_open2);
           
              var op1=res[0].valveopr_open2;
              var cl1=res[0].valveopr_close2;
              var per=((parseInt(op1) + parseInt(cl1))/2);
              document.getElementById("XX1").value=per;

              $("#XX1").val(per);
              localStorage.setItem("XX1",per);
          
            }
          });

          }
          else if(id=="valve_close1")
          {

           
            var get_location_name = $.ajax({
            type:"GET",
            url: "valveop.php?valveopr_id="+valveopr_id,
            success: function(result) {
              
              var res=JSON.parse(result).info;
              
              
             $("#valveopr_close1").val(res[0].valveopr_close1); 
            }
          });

          }
          else if (id=="valve_close2")
          {

            var get_location_name = $.ajax({
            type:"GET",
            url: "valveop.php?valveopr_id="+valveopr_id,
            success: function(result) {
              
              var res=JSON.parse(result).info;
              
              
             $("#valveopr_close2").val(res[0].valveopr_close2); 
            }
          });
          }

    });

function locationpara_location(){
          var sublocation_id = $('#subloc').val();
  
          var get_location_name = $.ajax({
            type:"GET",
            url: "dblocationpara.php?sublocation_id="+sublocation_id,
            success: function(result) {
            	var res=JSON.parse(result).info;
            	
              $("#tank_level").val(res[0].tank_level);
              localStorage.setItem("tank_level", res[0].tank_level);
              $("#flow_rate1").val(res[0].flow_rate1);
              localStorage.setItem("flow_rate1", res[0].flow_rate1);
              $("#flow_rate2").val(res[0].flow_rate2);
              localStorage.setItem("flow_rate2", res[0].flow_rate2);
              $("#line_pressure1").val(res[0].line_pressure1);
               localStorage.setItem("line_pressure1", res[0].line_pressure1);
              $("#line_pressure2").val(res[0].line_pressure2);
               localStorage.setItem("line_pressure2", res[0].line_pressure2);
              $("#record_time").val(res[0].record_time); /**/
              localStorage.setItem("record_time", res[0].record_time);
             
            }
      });

 }
     
</script>

</body>
</html>